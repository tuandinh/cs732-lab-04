import { AppContext } from './AppContextProvider';
import { useContext } from 'react';

function App() {

  return (

    <div>
      <h1>My Todo App!</h1>
    </div>
  );
}

export default App;